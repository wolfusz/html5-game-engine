GRAPHICS = new (function(){
    var source;
    
    this.width = 0;
    this.height = 0;
    this.init = function(a,b){
        this.source = a;
        this.source.canvas.width = window.innerWidth;
        this.source.canvas.height = window.innerHeight;
        this.width = this.source.canvas.width;
        this.height = this.source.canvas.height;
        
        this.backgroundColor = b;
        this.clear();
    };
    
    this.font = function(a){ if(a) this.source.font = a; else return this.source.font; };
    
    // DRAWING
    var _fill = false; /* getter */ this.fill = function(){return _fill;};
    var _pixel = { size: 10 }; /* getter */ this._pixel = function(){return _pixel.size;};
    
    this.draw = function(t,i){
        if(t && i){
            // drawing options (color,fill,line width)
            if(i.fill) fill = i.fill; else fill = _fill;
            if(i.style){
                this.source.fillStyle = i.style;
                this.source.strokeStyle = i.style;
            }
            
            switch(t){
                case "line":
                        if(i.coords){
                            if(! i.NPB){ // !NBP => BP (Pixel Based)
                                var dx = Math.abs(i.coords[2] - i.coords[0]), sx = i.coords[0] < i.coords[2] ? 1 : -1;
                                var dy = Math.abs(i.coords[3] - i.coords[1]), sy = i.coords[1] < i.coords[3] ? 1 : -1; 
                                var err = (dx>dy ? dx : -dy)/2;
                                while (true) {
                                    setPixel(x0,y0);
                                    if (x0 === x1 && y0 === y1) break;
                                    var e2 = err;
                                    if (e2 > -dx) { err -= dy; x0 += sx; }
                                    if (e2 < dy) { err += dx; y0 += sy; }
                                }
                            }else{
                                if(i.width) this.source.lineWidth = i.lineWidth;
                                this.source.beginPath();
                                this.source.moveTo(i.coords[0],i.coords[1]);
                                this.source.lineTo(i.coords[2],i.coords[3]);
                                this.source.stroke();
                            }   
                        }else ENGINE.error("Dafuq? How to draw this line?");
                    break;
                case "circle":
                        if(i.coords){
                            this.source.beginPath();
                            this.source.arc(i.coords[0],i.coords[1],i.coords[2],0,2*Math.PI,true);
                            if(fill)
                                this.source.fill();
                            else
                                this.source.stroke();
                        }else ENGINE.error("Dafuq? How to draw this circle?");
                    break;
                case "rect":
                        if(i.coords){
                            // do for both PixelBased and NPB coords
                            var x = i.NPB ? i.coords[0] : (i.coords[0]-i.coords[0]%_pixel.size)*_pixel.size;
                            var y = i.NPB ? i.coords[1] : (i.coords[1]-i.coords[1]%_pixel.size)*_pixel.size;
                            var w = i.NPB ? i.coords[2] : i.coords[2]*_pixel.size;
                            var h = i.NPB ? i.coords[3] : i.coords[3]*_pixel.size;
                            if(fill)
                                this.source.fillRect(x,y,w,h);
                            else
                                this.source.strokeRect(x,y,w,h);
                        }else
                            this.source.fillRect(0,0,this.width,this.height);
                    break;
                case "pixel":
                        if(i.coords){
                            i.coords[0] = i.coords[0]*_pixel.size;
                            i.coords[1] = i.coords[1]*_pixel.size;
                            this.source.fillRect(i.coords[0],i.coords[1],_pixel.size,_pixel.size);
                        }else ENGINE.error("Dafuq? Where to put pixel?");
                    break;
            }
        }else ENGINE.error("No drawing action or options specified.");
    };
    
    // CLEAR SCREEN
    this.clear = function(c){ this.draw("rect",{fill:true,style:c||this.backgroundColor}); };
    
    // DRAWING ALIASES
    this.rect = function(i){ this.draw("rect",i); };
    this.circle = function(i){ this.draw("circle",i); };
    this.line = function(i){ this.draw("line",i); };
    this.pixel = function(i){ this.draw("pixel",i); };
    
    // UTILITIES
    this.maxX = function(){return(this.width/_pixel.size);};
    this.maxY = function(){return(this.height/_pixel.size);};
    
    // DRAW ALL
    this.flush = function(){
        this.clear();
        WORLD.draw();
    };
})();