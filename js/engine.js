ENGINE = new (function(){
    var source;
    var loopTimer;
    var loop;
    
    this.init = function(a,b,c){
        this.source = a;

        GRAPHICS.init(this.source.getContext("2d"),b);
        WORLD.init(GRAPHICS.width/GRAPHICS._pixel.size/2,GRAPHICS.height/GRAPHICS._pixel.size/2,GRAPHICS.width/GRAPHICS._pixel.size,GRAPHICS.height/GRAPHICS._pixel.size);
        
        loop = c;
    };
    
    // LOOP SIMULATION
    this.start = function(){ this.loopTimer = setInterval(function(){loop();},10); };
    this.stop = function(){ clearInterval(this.loopTimer); };
    
    this.error = function(msg){ alert(msg); };
})();