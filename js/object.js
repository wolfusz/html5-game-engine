OBJECT = (function(o){
    this.pos = o.pos||{x:0,y:0};
    this.size = o.size||{x:1,y:1};
    this.color = o.color||"#000";
    this.draw = o.draw||function(){
        GRAPHICS.rect({
            NBP:true,
            fill:true,
            coords:[this.pos.x-this.size.x/2,this.pos.y-this.size.y/2,this.size.x,this.size.y],
            style:this.color
        });
    };
    this.process = o.process||function(){
        
    };
    
    if(o.image){
        this.image = new Image();
        this.image.src = o.image;
    }
    
    console.log(this.pos);
    console.log(this.size);
});

BUILDING = OBJECT;
BUILDING.draw = function(){
    GRAPHICS.rect({
        coords:[this.pos.x-this.size.x/2,this.pos.y-this.size.y/2,this.size.x,this.size.y],
        style:this.color
    });
    
    var windowW = 3;
    var windowH = 4;
    for(x = GRAPHICS._pixel(); x < this.size.x/GRAPHICS._pixel(); x += GRAPHICS._pixel()*(windowW+1))
        for(y = GRAPHICS._pixel(); y < this.size.y/GRAPHICS._pixel(); y += GRAPHICS._pixel()*(windowH+1)){
            GRAPHICS.rect({
                NBP:true,
                coords:[this.pos.x+x, this.pos.y+y, GRAPHICS._pixel()*windowW, GRAPHICS._pixel()*windowH],
                style:"#aaa"
            });
        }
};