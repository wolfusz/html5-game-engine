WORLD = new (function(){
    // world center
    this.origin = {x:0,y:0};
    this.size = {x:0,y:0};
    
    this.init = function(x,y,w,h){
        this.origin = {x:x,y:y};
        this.size = {x:w,y:h};
        this._atoms = [this.size.x*2,this.size.y*2];
    }
    
    this.draw = function(){
        _objects.forEach(function(entry){
            entry.draw();
        });
    };
    this.process = function(){
        _objects.forEach(function(entry){
            entry.process();
        });
    };
    
    // OBJECTS
    var _atoms = [,];
    var _objects = [];
    this.createObject = function(o){
        _objects.push(new OBJECT(o));
    };
    this.spawnBuilding = function(o){
        _objects.push(new BUILDING(o));
    };
})();